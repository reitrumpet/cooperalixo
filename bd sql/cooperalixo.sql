-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06-Jun-2018 às 02:24
-- Versão do servidor: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cooperalixo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `mat_delivery`
--

CREATE TABLE `mat_delivery` (
  `ID` int(11) NOT NULL,
  `ID_TYPE` int(11) NOT NULL,
  `QTD` int(11) NOT NULL,
  `MEASURE` varchar(50) NOT NULL,
  `ID_REG_USER` int(11) NOT NULL,
  `DATE` datetime NOT NULL,
  `SCORE` int(11) NOT NULL,
  `SITUATION` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `mat_delivery`
--

INSERT INTO `mat_delivery` (`ID`, `ID_TYPE`, `QTD`, `MEASURE`, `ID_REG_USER`, `DATE`, `SCORE`, `SITUATION`) VALUES
(9, 1, 1, 'KG', 25, '2018-05-06 00:00:00', 0, 'Cancelado'),
(10, 2, 1, 'KG', 25, '2018-05-06 00:00:00', 0, 'Cancelado'),
(11, 3, 1, 'KG', 25, '2018-05-06 00:00:00', 5, 'Aprovado'),
(12, 4, 1, 'KG', 25, '2018-05-06 00:00:00', 5, 'Aprovado'),
(13, 5, 1, 'KG', 25, '2018-05-06 00:00:00', 1, 'Aprovado'),
(14, 1, 10, 'KG', 25, '2018-05-06 00:00:00', 10, 'Aprovado'),
(15, 2, 10, 'KG', 25, '2018-05-06 00:00:00', 0, 'Pendente'),
(16, 3, 10, 'KG', 25, '2018-05-06 00:00:00', 0, 'Pendente'),
(17, 4, 10, 'KG', 25, '2018-05-06 00:00:00', 0, 'Pendente'),
(18, 5, 10, 'KG', 25, '2018-05-06 00:00:00', 0, 'Pendente'),
(19, 4, 15, 'KG', 25, '2018-05-06 00:00:00', 0, 'Pendente'),
(21, 1, 2, 'KG', 25, '2018-05-08 00:00:00', 0, 'Pendente'),
(22, 1, 1, 'KG', 25, '2018-05-13 00:00:00', 0, 'Pendente'),
(23, 1, 10, 'KG', 26, '2018-05-13 00:00:00', 10, 'Aprovado'),
(24, 2, 10, 'KG', 26, '2018-05-13 00:00:00', 30, 'Aprovado'),
(25, 3, 50, 'KG', 25, '2018-05-16 00:00:00', 250, 'Pendente'),
(26, 1, 10, 'KG', 25, '2018-06-04 00:00:00', 10, 'Pendente');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mat_type`
--

CREATE TABLE `mat_type` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(99) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `mat_type`
--

INSERT INTO `mat_type` (`ID`, `NAME`) VALUES
(1, 'paper'),
(2, 'plastic'),
(3, 'metal'),
(4, 'glass'),
(5, 'organic');

-- --------------------------------------------------------

--
-- Estrutura da tabela `reg_user`
--

CREATE TABLE `reg_user` (
  `ID` int(11) NOT NULL,
  `PASSWORD` varchar(99) COLLATE utf8_bin NOT NULL,
  `NAME` varchar(99) COLLATE utf8_bin NOT NULL,
  `EMAIL` varchar(99) COLLATE utf8_bin NOT NULL,
  `PHONE` varchar(99) COLLATE utf8_bin NOT NULL,
  `DATE_REG` date DEFAULT NULL,
  `CONFIRMATION` varchar(3) COLLATE utf8_bin NOT NULL,
  `CNPJ` varchar(18) COLLATE utf8_bin NOT NULL,
  `TYPE` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `reg_user`
--

INSERT INTO `reg_user` (`ID`, `PASSWORD`, `NAME`, `EMAIL`, `PHONE`, `DATE_REG`, `CONFIRMATION`, `CNPJ`, `TYPE`) VALUES
(25, '123', 'Reinaldo Medeiros', 'rei@bluedental.com.br', '(82)99909-7663', '2018-04-25', 'No', '', 1),
(26, '123', 'Raylan', 'raylanmedeiros@hotmail.com', '82999368273', '2018-04-25', 'No', '', 1),
(50, '123123123', 'Teste COOP', 'coop@teste.com', '(12)1 2121-2121', '2018-06-03', 'No', '12.312.312/3123-12', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mat_delivery`
--
ALTER TABLE `mat_delivery`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `ID_REG_USER` (`ID_REG_USER`),
  ADD KEY `ID_TYPE` (`ID_TYPE`);

--
-- Indexes for table `mat_type`
--
ALTER TABLE `mat_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `reg_user`
--
ALTER TABLE `reg_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mat_delivery`
--
ALTER TABLE `mat_delivery`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `reg_user`
--
ALTER TABLE `reg_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `mat_delivery`
--
ALTER TABLE `mat_delivery`
  ADD CONSTRAINT `ID_REG_USER` FOREIGN KEY (`ID_REG_USER`) REFERENCES `reg_user` (`ID`),
  ADD CONSTRAINT `ID_TYPE` FOREIGN KEY (`ID_TYPE`) REFERENCES `mat_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
