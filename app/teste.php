<?php

include("../openDatabase.php");
include("validated.php");

if (isset($_GET['id'])) {
    $id = $_GET['id'];
};


$id = $_SESSION['id'];

$sql = "SELECT ID_TYPE,SUM(QTD) FROM mat_delivery WHERE ID_REG_USER=25 GROUP BY ID_TYPE ASC";

$query = mysqli_query($strcon, "$sql") or die(mysqli_error($strcon));

$array = [
    1 => 'papel',
    2 => 'plástico',
    3 => 'metal',
    4 => 'vidro',
    5 => 'orgânico'
];


while ($row = mysqli_fetch_array($query)) {

    $type = ucfirst($array[$row['ID_TYPE']]);
    $qtd = $row['SUM(QTD)'];
    $quantidadeF = " ";

//    $arrayDados = [
//        1 => $type . " ",
//        2 => $qtd . " "
//    ];

$arrayQuantidade[] = $qtd;


//echo $arrayQuantidade;
//    foreach ($qtd as $quantidade)
//    {
//        $quantidadeF = $quantidadeF.$quantidade;
//    }

//    $qtdSeparado = $qtd . " ";

//    echo $qtd . "</br>";
//    echo $qtdSeparado;

//    $qtdSeparadoExplode = explode(" ", $qtdSeparado);

//    echo $qtdSeparadoExplode[0];

//    $stringDados = $arrayDados[1]." ".$arrayDados[2];
//
//
//$arrayDadosExplode = explode(" ", $stringDados);
//


//list($papel, $qtdPapel, $plastico, $qtdPlastico, $metal, $qtdMetal, $vidro, $qtdVidro, $organico, $qtdOrganico) = explode(":", $stringDados);



//    AQUI
//    $stringDadosExplode = explode(" ", $stringDados);
//
//    echo $stringDadosExplode[0];
//    echo $stringDadosExplode[1];
//
//    $stringTypeSeparado = $stringDadosExplode[0];
//    $stringQtdSeparado = $stringDadosExplode[1];

//    echo $stringTypeSeparado;
//    echo $stringQtdSeparado;

//    $stringTypeExplode = explode( " ", $stringTypeSeparado);
//    $stringQtdExplode = explode( " ", $stringQtdSeparado);
//
//    echo $stringTypeExplode[1];



};

$qtdPapel = $arrayQuantidade[0];
$qtdPlastico = $arrayQuantidade[1];
$qtdMetal = $arrayQuantidade[2];
$qtdVidro = $arrayQuantidade[3];
$qtdOrganico = $arrayQuantidade[4];

?>


<html>
<head>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        //carregando modulo visualization
        google.load("visualization", "1", {packages: ["corechart"]});

        //função de monta e desenha o gráfico
        function drawChart() {
//variavel com armazenamos os dados, um array de array's
//no qual a primeira posição são os nomes das colunas



            var data = google.visualization.arrayToDataTable([
                ['Entregas', 'Quando gosto dela'],
                ['Papel', <?php echo $qtdPapel; ?>],
                ['Plástico', <?php echo $qtdPlastico; ?>],
                ['Metal', <?php echo $qtdMetal; ?>],
                ['Vidro', <?php echo $qtdVidro; ?>],
                ['Orgânico', <?php echo $qtdOrganico; ?>],

            ]);

//opções para exibição do gráfico
            var options = {
                title: 'Entregas',//titulo do gráfico
                is3D: true // false para 2d e true para 3d o padrão é false
            };
//cria novo objeto PeiChart que recebe
//como parâmetro uma div onde o gráfico será desenhado
            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
//desenha passando os dados e as opções
            chart.draw(data, options);
        }

        //metodo chamado após o carregamento
        google.setOnLoadCallback(drawChart);
    </script>
</head>
<body>
<?php

include("sidebar.php");


?>


<div class="container top50">
    <div class="row border-groove padding20">
        <h2 class=""> Painel </h2>
        <div id="chart_div" style="width: 900px; height: 500px;"></div>
    </div>
</div>
<!-- Footer -->
<?php

$year = date('Y');


?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <p class="copyright text-muted">Copyright &copy; CooperaLixo - Rei Medeiros <?php echo $year; ?></p>
            </div>
        </div>
    </div>
</footer>

</body>

</html>