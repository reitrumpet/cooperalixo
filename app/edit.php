<?php include("header.php"); ?>
<?php include("sidebar.php"); ?>
    <!-- Início Conteúdo -->

    <div class="container top50">
    <div class="row border-groove padding20">
        <h2 class=""> Edite seu perfil </h2>
        <!--            <h4>Qual tipo de lixo voc&ecirc; deseja fazer uma entrega ?</h4>-->
        <div class="">
            <?php
            include("../openDatabase.php");

            $id = $_SESSION['id'];

            $sql = "SELECT
reg_user.ID,
reg_user.`PASSWORD`,
reg_user.`NAME`,
reg_user.EMAIL,
reg_user.PHONE,
reg_user.DATE_REG,
reg_user.CONFIRMATION,
reg_user.cep,
reg_user.cidade,
reg_user.bairro,
reg_user.logradouro,
reg_user.numero,
reg_user.complemento
FROM
reg_user
WHERE
reg_user.ID = $id";;
            $query = mysqli_query($strcon, $sql) or die(mysqli_error($strcon));

            while ($row = mysqli_fetch_array($query)){


            ?>

            <div class="site-heading">

                <form method="post" action="#">
                    <h4 class="login padding10">Registre-se</h4>
                    <div class="bottom10">
                        <div class="floatLeft">Nome*</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Nome" name="name"
                               type="text" value="<?php echo $row['NAME']; ?>" required/>
                    </div>
                    <div class="bottom10">
                        <div class="floatLeft">E-mail*</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Email" name="email"
                               type="email" value="<?php echo $row['EMAIL']; ?>" required/>
                    </div>
                    <div class="bottom10">
                        <div class="floatLeft">Senha*</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Senha"
                               name="password" type="password" value="<?php echo $row['PASSWORD']; ?>" required/>
                    </div>
                    <div class="bottom20">
                        <div class="floatLeft text-white fontRegister">Telefone</div>
                        <input class="text-white" id="phone" placeholder="Telefone" name="phone" value="<?php echo $row['PHONE']; ?>" required/>

                    </div>
                    <div class="bottom20">
                        <div class="floatLeft text-white fontRegister">Cep</div>
                        <input class="text-white" id="cep" placeholder="Cep" name="cep" value="<?php echo $row['cep']; ?>" required/>

                    </div>
                    <div class="bottom20">
                        <div class="floatLeft">Cidade*</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Cidade"
                               name="cidade" type="text" value="<?php echo $row['cidade']; ?>" required/>
                    </div>
                    <div class="bottom20">
                        <div class="floatLeft">Bairro*</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Bairro"
                               name="bairro" type="text" value="<?php echo $row['bairro']; ?>" required/>
                    </div>
                    <div class="bottom20">
                        <div class="floatLeft">Logradouro*</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Logradouro"
                               name="logradouro" type="text" value="<?php echo $row['logradouro']; ?>" required/>
                    </div>
                    <div class="bottom20">
                        <div class="floatLeft">Número*</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Número"
                               name="numero" type="text" value="<?php echo $row['numero']; ?>" required/>
                    </div>
                    <div class="bottom20">
                        <div class="floatLeft">Complemento</div>
                        <input size="60" maxlength="255" class="form-control" placeholder="Complemento"
                               name="complemento" type="text" value="<?php echo $row['complemento']; ?>" />
                    </div>
                    <input class="login padding10 cursorPointer" type="submit" value="Salvar">
                    <script type="text/javascript">
                        $("input[id*='cnpj']").inputmask({
                            mask: ['99.999.999/9999-99'],
                            keepStatic: true
                        });

                        $("input[id*='telefone']").inputmask({
                            mask: ['(99) 9999-9999','(99)9 9999-9999'],
                            keepStatic: true
                        });

                        $("input[id*='cep']").inputmask({
                            mask: ['99.999-999'],
                            keepStatic: true
                        });

                    </script>
                </form>

                <?php }
                if (count($_POST) > 0) {
                    $name = $_POST['name'];
                    $email = $_POST['email'];
                    $password = $_POST['password'];
                    $phone = $_POST['phone'];
                    $cep = $_POST['cep'];
                    $cidade = $_POST['cidade'];
                    $bairro = $_POST['bairro'];
                    $logradouro = $_POST['logradouro'];
                    $numero = $_POST['numero'];
                    $complemento = $_POST['complemento'];

                    $sql_update = "UPDATE `reg_user` SET `PASSWORD`=$password,`NAME`='$name',`EMAIL`='$email',`PHONE`='$phone',`cep`='$cep',`cidade`='$cidade',`bairro`='$bairro',`logradouro`='$logradouro',`numero`='$numero',`complemento`='$complemento' WHERE ID=$id";

                    if ($query_update = mysqli_query($strcon, $sql_update) or die(mysqli_error($strcon))) {
                        echo "<meta HTTP-EQUIV='refresh' CONTENT='0;URL=edit.php'>";
                        echo "<script> alert('Usuário atualizado com sucesso.');</script>";
                        mysqli_close($strcon);
                    } else {
                        echo "<script> alert('Usuário não atualizado.');</script>";
                    }
                }
                ?>
            </div>
        </div>
    </div>


    <!-- Fim Conteúdo -->
<?php include("footer.php"); ?>