<?php

include("../../openDatabase.php");
include("../validated.php");

if (isset($_GET['id'])) {
    $id = $_GET['id'];
};

error_reporting(0);
ini_set(“display_errors”, 0 );

$id = $_SESSION['id'];

$sql = "SELECT ID_TYPE,SUM(QTD) FROM mat_delivery WHERE ID_REG_USER=$id GROUP BY ID_TYPE ASC";

$query = mysqli_query($strcon, "$sql") or die(mysqli_error($strcon));

$array = [
    1 => 'papel',
    2 => 'plástico',
    3 => 'metal',
    4 => 'vidro',
    5 => 'orgânico'
];


while ($row = mysqli_fetch_array($query)) {

    $type = ucfirst($array[$row['ID_TYPE']]);
    $qtd = $row['SUM(QTD)'];
    $quantidadeF = " ";

//    $arrayDados = [
//        1 => $type . " ",
//        2 => $qtd . " "
//    ];

    $arrayQuantidade[] = $qtd;


};

//$qtdPapel = $arrayQuantidade[0];
if($arrayQuantidade[0]>0){$qtdPapel = $arrayQuantidade[0];}else{$qtdPapel = 0;};
//$qtdPlastico = $arrayQuantidade[1];
if($arrayQuantidade[1]>0){$qtdPlastico = $arrayQuantidade[1];}else{$qtdPlastico = 0;};
//$qtdMetal = $arrayQuantidade[2];
if($arrayQuantidade[2]>0){$qtdMetal = $arrayQuantidade[2];}else{$qtdMetal = 0;};
//$qtdVidro = $arrayQuantidade[3];
if($arrayQuantidade[3]>0){$qtdVidro = $arrayQuantidade[3];}else{$qtdVidro = 0;};
//$qtdOrganico = $arrayQuantidade[4];
if($arrayQuantidade[4]>0){$qtdOrganico = $arrayQuantidade[4];}else{$qtdOrganico = 0;};


?>


<html>
<head>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        //carregando modulo visualization
        google.load("visualization", "1", {packages: ["corechart"]});

        //função de monta e desenha o gráfico
        function drawChart() {
//variavel com armazenamos os dados, um array de array's
//no qual a primeira posição são os nomes das colunas



            var data = google.visualization.arrayToDataTable([
                ['Entregas', 'Quando gosto dela'],
                ['Papel', <?php echo $qtdPapel; ?>],
                ['Plástico', <?php echo $qtdPlastico; ?>],
                ['Metal', <?php echo $qtdMetal; ?>],
                ['Vidro', <?php echo $qtdVidro; ?>],
                ['Orgânico', <?php echo $qtdOrganico; ?>],

            ]);

//opções para exibição do gráfico
            var options = {
                title: 'Entregas',//titulo do gráfico
                is3D: true // false para 2d e true para 3d o padrão é false
            };
//cria novo objeto PeiChart que recebe
//como parâmetro uma div onde o gráfico será desenhado
            var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
//desenha passando os dados e as opções
            chart.draw(data, options);
        }

        //metodo chamado após o carregamento
        google.setOnLoadCallback(drawChart);
    </script>
</head>
<body>
<?php

include("sidebar.php");


?>


<div class="container top50">
    <div class="row border-groove padding20">
        <h2 class=""> Painel </h2>
        <div id="chart_div" style=" height: 500px;"></div>
    </div>
</div>
<!-- Footer -->
<?php

$year = date('Y');


?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <p class="copyright text-muted">Copyright &copy; CooperaLixo - Rei Medeiros <?php echo $year; ?></p>
            </div>
        </div>
    </div>
</footer>


</body>

</html>