<?php include("header.php");
?>
<?php include("sidebar.php"); ?>
    <!-- Início Conteúdo -->

    <div class="container top50">
        <div class="row border-groove padding20">
            <h2 class=""> Hist&oacute;rico de Entregas </h2>
            <h4>Veja as entregas que voc&ecirc; fez at&eacute; o momento.</h4>
            <div class="top30">

                <?php

                include("../../openDatabase.php");

                $id = $_SESSION['id'];
                $total_reg = "10"; // número de registros por página

                if (isset($_GET['pagina'])) {
                    $pagina = $_GET['pagina'];
                } else {
                    $pagina = 1;
                }

                if (!$pagina) {
                    $pc = "1";
                } else {
                    $pc = $pagina;
                }

                $inicio = $pc - 1;
                $inicio = $inicio * $total_reg;

                $sql = "SELECT
mat_delivery.ID,
mat_delivery.ID_TYPE, 
mat_delivery.QTD,
mat_delivery.MEASURE,
mat_delivery.DATE,
mat_delivery.SCORE,
mat_delivery.SITUATION
FROM
mat_delivery
WHERE
mat_delivery.ID_REG_USER = $id";

                $limite = mysqli_query($strcon, "$sql LIMIT $inicio, $total_reg") or die(mysqli_error($strcon));

                $array = [
                    1 => 'papel',
                    2 => 'plástico',
                    3 => 'metal',
                    4 => 'vidro',
                    5 => 'orgânico'
                ];

                $arrayIcon = [
                    'Pendente' => '<img class="iconSituation" alt="Pendente" title="Pendente" src="../../img/clock.png">',
                    'Aprovado' => '<img class="iconSituation" alt="Aprovado" title="Aprovado" src="../../img/checked.png">',
                    'Cancelado' => '<img class="iconSituation" alt="Cancelado" title="Cancelado" src="../../img/cancel.png">'
                ];

                $todos = mysqli_query($strcon, "$sql") or die(mysqli_error($strcon));

                $tr = mysqli_num_rows($todos); // verifica o número total de registros
                $tp = $tr / $total_reg; // verifica o número total de páginas

                echo "<table class='table table-bordered table-responsive'>";
                echo "<tr class='height50'>";
                echo "<td class='negrito'>Data</td>";
                echo "<td class='negrito'>Tipo</td>";
                echo "<td class='negrito'>Quantidade</td>";
                echo "<td class='negrito'>Situação</td>";
                echo "</tr>";
                // vamos criar a visualização
                while ($row = mysqli_fetch_array($limite)) {
                    $dt = DateTime::createFromFormat('Y-m-d H:i:s', $row['DATE']);
                    $date = $dt->format('d/m/Y');
                    $situation = $row['SITUATION'];
                    echo "<tr class='height50'>";
                    echo "<td>" . $date . "</td>";
                    echo "<td>" . ucfirst($array[$row['ID_TYPE']]) . "</td>";

                    echo "<td>" . $row['QTD'] . "</td>";

                    echo "<td class='text-center'>" . $arrayIcon[$row['SITUATION']] . "</td>";
                    echo "</tr>";
                }

                // agora vamos criar os botões "Anterior e próximo"
                $anterior = $pc -1;
                $proximo = $pc +1;
                if ($pc>1) {
                    echo " <a href='?pagina=$anterior'><- Anterior</a> ";
                }
                echo "|";
                if ($pc<$tp) {
                    echo " <a href='?pagina=$proximo'>Próxima -></a>";
                }


                //                $query = mysqli_query($strcon, $sql) or die(mysqli_error($strcon));
                //                echo "<table class='table table-bordered table-responsive'>";
                //                echo "<tr class='height50'>";
                //                echo "<td class='negrito'>Data</td>";
                //                echo "<td class='negrito'>Tipo</td>";
                //                echo "<td class='negrito'>Situação</td>";
                //                echo "</tr>";
                //                while ($row = mysqli_fetch_array($query)) {
                //
                //                    $dt = DateTime::createFromFormat('Y-m-d H:i:s', $row['DATE']);
                //                    $date = $dt->format('d/m/Y');
                //
                //                    echo "<tr class='height50'>";
                //                    echo "<td>". $date ."</td>";
                //                    echo "<td>". ucfirst($array[$row['ID_TYPE']]) ."</td>";
                //                    echo "<td class='text-center'>" . $arrayIcon[$row['SITUATION']] . "</td>";
                //                    echo "</tr>";
                //                }


                mysqli_close($strcon);
                echo "</table>"

                ?>

            </div>
        </div>
    </div>


    <!-- Fim Conteúdo -->
<?php include("footer.php"); ?>