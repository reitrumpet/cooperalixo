<?php

include "header.php";

?>
<header class="masthead" style="background-image: url('img/contact-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="page-heading">
                    <h1>Contato</h1>
                    <span class="subheading">Tem alguma pergunta? Eu tenho a resposta :).</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <p>Quer entrar em contato ? Preencha o formul&aacute;rio para me enviar uma mensagem e eu entrarei em
                contato com voc&ecirc; o mais breve poss&iacute;vel!</p>
            <!-- Contact Form - Enter your email address on line 19 of the mail/contact_me.php file to make this form work. -->
            <!-- WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! -->
            <!-- To use the contact form, your site must be on a live web host with PHP! The form will not work locally! -->
            <form name="sentMessage" id="contactForm" method="post" action="#">
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Nome</label>
                        <input type="text" class="form-control" placeholder="Nome" name="name" id="name" required>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Email</label>
                        <input type="email" class="form-control" placeholder="E-mail" name="email" id="email" required>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group col-xs-12 floating-label-form-group controls">
                        <label>Telefone</label>
                        <input type="tel" class="form-control" placeholder="Telefone" name="phone" id="phone" required>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Mensagem</label>
                        <textarea rows="5" class="form-control" placeholder="Mensagem" name="message" id="message"
                                  required></textarea>
                        <p class="help-block text-danger"></p>
                    </div>
                </div>
                <br>
                <div id="success"></div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" id="sendMessageButton">Enviar</button>
                </div>
            </form>


            <?php
            if (count($_POST) > 0) {
                $name = $_POST['name'];
                $email = $_POST['email'];
                $phone = $_POST['phone'];
                $message = $_POST['message'];

                $from = $email;
                $to = "rm091291@gmail.com";
                $subject = "Contato CooperaLixo:" . $name;
                $mensagem = $message;
                $headers = "De:" . $from;
               echo $enviaremail = mail($to, $subject, $mensagem, $headers);


//                if ($enviaremail) {
//                    echo "<script> alert('Mensagem enviada com sucesso.');</script>";
//                } else {
//                    echo "<script> alert('A mensagem não foi enviada.');</script>";
//                }
            }

            //            echo $name;
            //            echo $email;
            //            echo $phone;
            //            echo $message;


            //            if (isset($_GET["emailSend"])){
            //
            //                $emailSend = $_GET["emailSend"];
            //                if ($emailSend == "no"){
            //                    echo "<script> alert('A mensagem não foi enviada.');</script>";
            //                }elseif($emailSend == "yes"){
            //                    echo "<script> alert('Mensagem enviada com sucesso.');</script>";
            //                }
            //
            //            }


            ?>
        </div>
    </div>
</div>

<hr>

<?php

include "footer.php";

?>
