<?php

include "header.php";

//$url = (isset($_GET['url'])) ? $_GET['url']:'index.php';
//$url = array_filter(explode('/',$url));
//
//$file = $url[0] .'.php';
//
//if (is_file($file)){
//    include $file;
//}else{
//    include '404.php';
//}

?>
    <!-- Begin Posts -->
    <header class="masthead" style="background-image: url('img/home-bg.jpg')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="site-heading">
                        <h1>Cooperalixo</h1>
                        <span class="subheading">Coleta Inteligente</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <!--
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">
                    <div class="post-preview">
                        <a href="posts.php">
                            <h2 class="post-title">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </h2>
                            <h3 class="post-subtitle">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </h3>
                        </a>
                        <p class="post-meta">Postado por
                            <a href="#">Lorem ipsum</a>
                            em 13 abril, 2018</p>
                    </div>
                    <hr>
                    <div class="post-preview">
                        <a href="posts.php">
                            <h2 class="post-title">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </h2>
                            <h3 class="post-subtitle">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit
                            </h3>
                        </a>
                        <p class="post-meta">Postado por
                            <a href="#">Lorem ipsum</a>
                            em 13 abril, 2018</p>
                    </div>
                    <hr>-->
    <!-- Pager -->
    <!--
                    <div class="clearfix">
                        <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
                    </div>
                </div>
            </div>
        </div>

        <hr>
    -->
    <!-- End Posts -->

    <!-- Begin About -->
    <div class="container" id="about">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <h2>Sobre</h2>
                <br/>
                <article class="about">
                    <p>O Cooperalixo foi criado visando inovar a forma de reciclagem com intuito direto de fazer com que
                        a
                        população trabalhe em conjunto com os responsáveis pela reciclagem na cidade.
                        Com apenas alguns dados do cidadão fornecidos, a cooperativa vai até a sua residência para
                        buscar os
                        materiais recicláveis e diminuir o lixo direcionado aos aterros sanitários.
                    </p>
                    <p>
                        O projeto foi idealizado pela turma do 3º "C" ano da Escola Estadual Dom Constantino Lüers e desenvolvido por Reinaldo Medeiros.
                    </p>

                </article>
                <br/>
                <div id="partners">
                    <?php include("partners.php"); ?>
                </div>

            </div>
        </div>
    </div>
    <!-- End About -->

<?php

include "footer.php";

?>